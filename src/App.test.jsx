import React from "react";
import { shallow } from "enzyme";
import App from "@/App";

const wrapped = shallow(<App />);

describe("App", () => {
  it("should render correctly", () => {
    expect(wrapped).toMatchSnapshot();
  });

  it("should render h1 title", () => {
    expect(wrapped.find("h1").text()).toEqual(" Hello, World! ");
  });
});
