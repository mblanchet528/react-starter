module.exports = {
  setupFiles: ["<rootDir>/enzyme.setup.js"],
  moduleFileExtensions: ["js", "jsx"],
  moduleDirectories: ["node_modules", "bower_components"],
  moduleNameMapper: {
    "\\.(css|less)$": "identity-obj-proxy",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/testUtils/fileMock.js",

    "^@/(.*)$": "<rootDir>/src/$1",
  },
  snapshotSerializers: ["enzyme-to-json/serializer"],
};
